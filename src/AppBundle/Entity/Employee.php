<?php
/**
 * Created by PhpStorm.
 * User: asidorov
 * Date: 02.03.2018
 * Time: 14:51
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployeeRepository")
 * @ORM\Table(name="eployees")
 * @UniqueEntity(
 *     fields={"number"},
 *     message="Employee with this number already exist."
 * )
 */
class Employee
{

    /**
     * @ORM\Id
     * @ORM\Column(name="number", type="string", length=10, nullable=false)
     * @Assert\NotBlank(message="Поле Номес")
     */
    protected $number;

    /**
     * @ORM\Column(name="fio", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    protected $fio;

    /**
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    protected $note;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Calendar", mappedBy="employee")
     */
    protected $calendar;

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Employee
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set fio
     *
     * @param string $fio
     *
     * @return Employee
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get fio
     *
     * @return string
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Employee
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->calendar = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add calendar
     *
     * @param \AppBundle\Entity\Calendar $calendar
     *
     * @return Employee
     */
    public function addCalendar(\AppBundle\Entity\Calendar $calendar)
    {
        $this->calendar[] = $calendar;

        return $this;
    }

    /**
     * Remove calendar
     *
     * @param \AppBundle\Entity\Calendar $calendar
     */
    public function removeCalendar(\AppBundle\Entity\Calendar $calendar)
    {
        $this->calendar->removeElement($calendar);
    }

    /**
     * Get calendar
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendar()
    {
        return $this->calendar;
    }
}
