<?php
/**
 * Created by PhpStorm.
 * User: asidorov
 * Date: 02.03.2018
 * Time: 14:51
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CalendarRepository")
 * @ORM\Table(name="calendar")
 */
class Calendar
{
    const STATUSES = [
        'duty' => 'дежурство в приемном отделении',
        'operating' => 'работа в операционной',
        'ill' => 'больничный',
        'vacation' => 'отпуск'
    ];
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(value="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    protected $date;

    /**
     * @ORM\Column(name="hours", type="integer", nullable=false)
     */
    protected $hours;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('operating', 'duty', 'ill', 'vacation')")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Employee", inversedBy="calendar")
     * @ORM\JoinColumn(name="employee_number", referencedColumnName="number", onDelete="CASCADE")
     */
    protected $employee;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Calendar
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set hours
     *
     * @param integer $hours
     *
     * @return Calendar
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return integer
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set employee
     *
     * @param \AppBundle\Entity\Employee $employee
     *
     * @return Calendar
     */
    public function setEmployee(\AppBundle\Entity\Employee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \AppBundle\Entity\Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Calendar
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
