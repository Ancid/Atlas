<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Calendar;
use AppBundle\Form\CalendarType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CalendarController extends Controller
{
    /**
     * Create Calendar
     * Создание записи колендаря
     *
     * @Route("/calendar", name="create_calendar")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     *
     * @api {put} /calendar/{number}         Create calendar record
     *
     * @example {json} Пример запроса:
     *      {
     *          "number": "25000001",
     *          "date": "01-01-2018"
     *          "hours": 8,
     *          "number": "duty",
     *      }
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'CREATED'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Employee number is empty."
     *     }
     */
    public function createAction(Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);

            if (!isset($params['number'], $params['date'], $params['hours'], $params['status'])) {
                throw new \Exception('Employee number is empty.');
            }
            $number = $params['number'];

            $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->findOneBy([
                'number' => $number
            ]);

            if (empty($employee)) {
                throw new \Exception('Employee not found.');
            }

            //проверка что не больше 8 часов на эту дату
            $valid = $this->getDoctrine()->getRepository('AppBundle:Calendar')->checkHoursForDate(
                $params['number'],
                $params['date'],
                $params['hours']
            );

            if (!$valid) {
                throw new \Exception('You can not record more than 8 hours a day');
            }

            unset($params['number']);
            $calendar = new Calendar();
            $form = $this->createForm(CalendarType::class, $calendar);
            $form->submit($params);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($calendar);
                $em->flush();
                $response = [
                    'result' => 'CREATED'
                ];
            } else {
                $response = [
                    'error' => (string)$form->getErrors(true)
                ];
            }

        }  catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Update calendar record
     * Обновление записи календаря
     *
     * @Route("/calendar/{id}", name="update_calendar")
     * @Method({"PUT"})
     * @param Request $request
     * @return JsonResponse
     *
     * @api {put} /calendar/{id}            Update calendar record
     *
     * @example {json} Пример запроса:
     *      {
     *          "hours": 4,
     *          "number": "duty",
     *      }
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'UPDATED'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: You can not record more than 8 hours a day."
     *     }
     */
    public function updateAction($id, Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);

            if (isset($params['number'])) {
                $number = $params['number'];
                unset($params['number']);

                $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->findOneBy([
                    'number' => $number
                ]);

                if (empty($employee)) {
                    throw new \Exception('Employee not found.');
                }
            }

            $calendar = $this->getDoctrine()->getRepository('AppBundle:Calendar')->findOneBy([
                'number' => $id
            ]);

            if (null === $calendar) {
                throw new \Exception('Incorrect entry id.');
            }

            //проверка что не больше 8 часов на эту дату
            $valid = $this->getDoctrine()->getRepository('AppBundle:Calendar')->checkHoursForDate(
                $params['number'],
                $params['date'],
                $params['hours']
            );

            if (!$valid) {
                throw new \Exception('You can not record more than 8 hours a day.');
            }

            $form = $this->createForm(CalendarType::class, $calendar);
            $form->submit($params);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $response = [
                    'result' => 'UPDATED'
                ];
            } else {
                $response = [
                    'error' => (string)$form->getErrors(true)
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Delete calendar record
     * Удаление заипси календаря
     *
     * @Route("/calendar/{id}", name="delete_calendar")
     * @Method({"DELETE"})
     * @param Integer $id                    ID записи календаря
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \LogicException
     *
     * @api {put} /calendar/{number}         Delete calendar record
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'DELETED'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Record not found."
     *     }
     */
    public function deleteAction($id, Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $calendar = $this->getDoctrine()->getRepository('AppBundle:Calendar')->findOneBy([
                'id' => $id
            ]);

            if (empty($calendar)) {
                throw new \Exception('Record not found.');
            }

            $em->remove($calendar);
            $em->flush();

            $response = [
                'result' => 'DELETED'
            ];
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Get Calendar list with filter [number, date, date_to]
     * Получение записей календаря с фильтром [number, date, date_to]
     *
     * @Route("/calendar", name="get_calendar")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \LogicException
     *
     * @api {get} /calendar                  Get Calendar list
     *
     * @example {json} Пример запроса:
     *    GET /calendar?number=25000001
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": [
     *              {
     *                  "date": {
     *                      "date": "2018-03-12 00:00:00.000000",
     *                      "timezone_type": 3,
     *                      "timezone": "Europe/Berlin"
     *                  },
     *                  "hours": 4,
     *                  "status": "duty",
     *                  "employee": "42007258"
     *              },
     *              {
     *                  "date": {
     *                      "date": "2018-03-12 00:00:00.000000",
     *                      "timezone_type": 3,
     *                      "timezone": "Europe/Berlin"
     *                  },
     *                  "hours": 4,
     *                  "status": "operating",
     *                  "employee": "42007258"
     *              }
     *          ]
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Record not found."
     *     }
     */
    public function listAction(Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);
            $filter = [];

            if (isset($params['number'])) {
                $filter['number'] = $params['number'];
            }
            if (isset($params['date'])) {
                $filter['date'] = $params['date'];
            }
            if (isset($params['date_to'])) {
                $filter['date_to'] = $params['date_to'];
            }
            $records = $this->getDoctrine()->getRepository('AppBundle:Calendar')->findByFilter($filter);

            $response = [
                'result' => $records
            ];
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Get statistic with filter [number, date, date_to]
     * Получение статистики с фильтром [number, date, date_to]
     *
     * @Route("/statistic", name="get_statistic")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \LogicException
     *
     * @api {get} /calendar                  Get statistic
     *
     * @example {json} Пример запроса:
     *    GET /statistic?number=25000001
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *         "result": {
     *              "42007258": {
     *                  "fio": "Alison Lasko",
     *                  "duty": [
     *                      {
     *                      "date": {
     *                          "date": "2018-03-12 00:00:00.000000",
     *                          "timezone_type": 3,
     *                          "timezone": "Europe/Berlin"
     *                      },
     *                      "hours": 4,
     *                      "status": "duty"
     *                      }
     *                  ],
     *                  "operating": [
     *                      {
     *                          "date": {
     *                          "date": "2018-03-12 00:00:00.000000",
     *                          "timezone_type": 3,
     *                          "timezone": "Europe/Berlin"
     *                      },
     *                      "hours": 4,
     *                      "status": "operating"
     *                      }
     *                  ]
     *              }
     *          }
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Invalid params."
     *     }
     */
    public function statisticAction(Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);
            $filter = [];

            if (isset($params['number'])) {
                $filter['number'] = $params['number'];
            }
            if (isset($params['date'])) {
                $filter['date'] = $params['date'];
            }
            if (isset($params['date_to'])) {
                $filter['date_to'] = $params['date_to'];
            }
            $statistic = $this->getDoctrine()->getRepository('AppBundle:Calendar')->statisticWithFilter($filter);

            $sorted = [];
            foreach ($statistic as $entry) {
                $number = $entry['employee'];
                $sorted[$number]['fio'] = $entry['fio'];
                unset($entry['employee'], $entry['fio']);
                $sorted[$number][$entry['status']][] = $entry;
            }

            $response = [
                'result' => $sorted
            ];
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }
}
