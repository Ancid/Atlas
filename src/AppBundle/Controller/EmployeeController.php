<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employee;
use AppBundle\Form\EmployeeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EmployeeController extends Controller
{
    /**
     * Create Employee
     * Создание нового сотрудника
     *
     * @Route("/employee", name="create_employee")
     * @Method({"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     *
     * @api {post} /employee                    Create employee
     *
     * @example {json} Пример запроса:
     *      {
     *          "number": "52007019",
     *          "fio": "Edward Norton",
     *          "note": "Испытательный срок"
     *      }
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'CREATED'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Employee with this number already exist."
     *     }
     */
    public function createAction(Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);
            $employee = new Employee();

            $form = $this->createForm(EmployeeType::class, $employee);
            $form->submit($params);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($employee);
                $em->flush();
                $response = [
                    'result' => 'CREATED'
                ];
            } else {
                $response = [
                    'error' => (string)$form->getErrors(true)
                ];
            }
        }  catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Update Employee
     * Обновление данных сотрудника
     *
     * @Route("/employee/{number}", name="update_employee")
     * @Method({"PUT"})
     * @param Integer number                 Табельный номер сотрудника
     * @return JsonResponse
     *
     *
     * @api {put} /employee/{number}         Update employee
     *
     *
     * @example {json} Пример запроса:
     *      {
     *          "fio": "Edward Norton",
     *          "note": "Испытательный срок пройден успешно"
     *      }
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'UPDATED'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Employee not found."
     *     }
     */
    public function updateAction($number, Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);
            $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->findOneBy([
                'number' => $number
            ]);

            $form = $this->createForm(EmployeeType::class, $employee);
            $form->submit($params, false);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $response = [
                    'result' => 'UPDATED'
                ];
            } else {
                $response = [
                    'error' => (string)$form->getErrors(true)
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Delete Employee
     * Удаление сотрудника
     *
     * @Route("/employee/{number}", name="delete_employee")
     * @Method({"DELETE"})
     * @param String number                  Табельный номер сотрудника
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @api {delete} /employee/{number}      Delete employee
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'DELETED'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Employee not found."
     *     }
     */
    public function deleteAction($number)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->findOneBy([
                'number' => $number
            ]);

            if (null === $employee) {
                throw new \Exception('Employee not found.');
            }

            $em->remove($employee);
            $em->flush();

            $response = [
                'result' => 'DELETED'
            ];
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Get Employee list with filter [number, fio]
     * Список сотрудников c фильтром [number, fio]
     *
     * @Route("/employee", name="get_employees")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     * @api {get} /employee                  Get employee list
     *
     *
     * @example {json} Пример запроса:
     *    GET /employee?number=25000001
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'DELETED'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Employee not found."
     *     }
     */
    public function listAction(Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);
            $filter = [];

            if (isset($params['number'])) {
                $filter['number'] = $params['number'];
            }
            if (isset($params['fio'])) {
                $filter['fio'] = $params['fio'];
            }
            $employees = $this->getDoctrine()->getRepository('AppBundle:Employee')->findArrayBy($filter);
            $response = [
                'employees' => $employees
            ];
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }


    /**
     * Create employees from excel file
     * Создание сотрудников из excel-файла
     *
     *
     * @Route("/employee/upload", name="upload_employees")
     * @Method({"POST"})
     * @return JsonResponse
     * @throws \HttpException
     *
     *
     * @api {post} /employee/upload             Get employee list
     *
     * @example {json} Пример запроса:
     *    {
     *        "number": "2506988",
     *        "fio": "Edward Norton"
     *    }
     *
     * @example {json} Положительный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "result": 'SUCCESS'
     *     }
     *
     * @example {json} Отрицательный ответ:
     *     HTTP/1.1 200 OK
     *     {
     *          "errors": "ERROR: Employee not found."
     *     }
     */
    public function uploadAction(Request $request)
    {
        try {
            if ($request->isXmlHttpRequest() && !$request->isMethod('POST')) {
                throw new \HttpException('XMLHttpRequests/AJAX calls must be POSTed');
            }

            /* @var UploadedFile */
            $file = $request->files->get('data-file');

            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($file->getPathname());
            $sheet = $phpExcelObject->getSheet(0);
            $highestRow = $sheet->getHighestRow();

            $em = $this->getDoctrine()->getManager();
            //Кэшируем сотрудников которых добавим
            $added = [];
            for ($row = 2; $row <= $highestRow; $row++){
                $i_row = $sheet->rangeToArray('A' . $row . ':C' . $row, NULL, TRUE, TRUE);

                if (!in_array($i_row[0][0], $added)) {
                    $employee = $em->getRepository('AppBundle:Employee')->findOneBy(['number' => $i_row[0][0]]);
                    if (null === $employee) {
                        $employee = new Employee();
                    }

                    $employee
                        ->setNumber($i_row[0][0])
                        ->setFio($i_row[0][1])
                        ->setNote($i_row[0][2]);
                    $added[] = $i_row[0][0];
                    $em->persist($employee);
                }
            }
            $em->flush();
            $response = [
                'result' => 'SUCCESS'
            ];
        } catch (\Exception $e) {
            $response = [
                'error' => $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }
}
